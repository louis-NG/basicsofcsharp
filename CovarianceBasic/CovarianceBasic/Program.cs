﻿using System;

namespace CovarianceBasic
{
    class Program
    {
        public delegate Small covarDel(Big C);
        public class Small
        {

        }
        public class Big : Small
        {

        }
        public class Bigger : Big
        {

        }
        public static Big Method1(Big _big)
        {
            Console.WriteLine("Method 1");
            return new Big();
        }
        public static Small Method2(Big _big)
        {
            Console.WriteLine("Method 2");
            return new Small();
        }
        static void Main(string[] args)
        {
            covarDel _covarDel = Method1;

            Small _big = _covarDel(new Big());

            _covarDel = Method2;
            Small _small = _covarDel(new Big());
        }
    }
}
