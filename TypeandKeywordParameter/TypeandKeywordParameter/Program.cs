﻿using System;
using System.Collections.Generic;

namespace TypeandKeywordParameter
{
     class Program
    {
        public static void PassInValueParameter(in int item)
        {
            int temp = item * 10;
           // temp = temp*10;
            // If method called ?\ Has item value changed ? Let's check ! 
        }
        public static void PassInReferenceParameter(in string item)
        {
            string temp = item.Substring(0, 3);
           // temp = temp.Substring(0,3);
            // If method called ? Has item Reference changed ? Let's check ! 
        }
        public static void PassOutValueParameter(out int item)
        {
            item = 9; // if you don't assign value for item, method will be error, because Out keyword obligate assign value for parameter before return. 
             item = item * 10;
            //temp = temp*10;
        }
        public static void PassOutReferenceParameter(out string item)
        {
            
           item = "Value"; // if you don't assign value for item, method will be error, because Out keyword obligate assign value for parameter before return. 
            string temp = item;
            temp.Split("u", 10);
            //temp =temp.Substring(0,3);
        }
        public static void PassRefValueParameter(ref int item)
        {
           item = item*10;
           // temp = temp *10;
        }
        public static void PassRefReferenceParameter(ref string item)
        {
            string temp = item;
            temp.Split("u", 10);
           // temp = temp.Substring(0, 3);
        }
        public static void PassListParameter(ref List<int> item)
        {
            List<int> temp = item;
            temp.RemoveAt(0);
            // temp = temp.Substring(0, 3);
        }


        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            int inItem = 1;
            string inItem1 = "Value initial";
            // Call method In keyword 
            PassInValueParameter(in inItem);
            Console.WriteLine(inItem + " Method PassInValueParameter called : " + inItem);
            PassInReferenceParameter(in inItem1);
            Console.WriteLine(inItem1 + " Method PassInReferenceParameter called : " + inItem1);
            // Call method Out keyword
            inItem = 1;
            inItem1 = "Value initial";
            PassOutValueParameter(out inItem);
            Console.WriteLine(inItem +  " Method PassOutValueParameter called : " + inItem);
            PassOutReferenceParameter(out inItem1);
            Console.WriteLine(inItem1 + " Method PassOutReferenceParameter called : " + inItem1);
            // Call method Ref keyword 
             inItem = 1;
             inItem1 = "Value initial";
            PassRefValueParameter(ref inItem);
            Console.WriteLine(inItem + " Method PassRefValueParameter called : " + inItem);
            PassRefReferenceParameter(ref inItem1);
            Console.WriteLine(inItem1 + " Method PassRefReferenceParameter called : " + inItem1);
            // Call method List
            List<int> list = new List<int>() { 1, 2, 3, 4, 5, 6, 7 };
            PassListParameter(ref list);
            Console.WriteLine(list.Count + " Method PassRefReferenceParameter called : " + list.Count);
        }
    }
}
