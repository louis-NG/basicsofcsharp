﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace StringClass
{
    class Program
    {
        public static void getAddressMemory(object obj)
        {
            GCHandle gch = GCHandle.Alloc(obj, GCHandleType.Pinned);
            IntPtr pObj = gch.AddrOfPinnedObject();
            Console.WriteLine(obj.ToString()+":"+pObj.ToString());
        }
        
       
        
        static void Main(string[] args)
        {
            string string1 = "Hi, This is string";
            int int1 = 2;

            StringBuilder string2 = new StringBuilder("Hi, this is stringbuilder");
            getAddressMemory(string1);
            string1 = "hi louis nguyen";
            getAddressMemory(string1);
            getAddressMemory(int1);
            int1 = 5;
            getAddressMemory(int1);
            int1 = 7;
            getAddressMemory(int1);
            //getAddressMemory(string2);
            DateTime datetime = DateTime.Now;
            double doublea = 75674.73789621;
            string doubleastring = string.Format("doublea {0:n3}", doublea);
            Console.WriteLine(doubleastring);
            Console.WriteLine("Current time {0}", datetime);


        }
    }
}
