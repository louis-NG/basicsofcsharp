﻿using System;

namespace ActionDelegate
{
    class Program
    {
        static void Print(int value)
        {
            Console.WriteLine("Output: " + value);
        }
      
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Action<int> _print = Print;
            _print(20);
            Action<int, int> _sum = delegate (int x, int y)
            {
                Console.WriteLine("Sum = {0}", x + y);
            };
            _sum(10, 20);
            Action<int> _output = (x) => Console.WriteLine("Number :{0}",x);
            _output(10);
         }
    }
}
