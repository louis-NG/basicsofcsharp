﻿using System;

namespace AnonymousMethod
{
    class Program
    {
        public delegate void Print(int value);
        static void Main(string[] args)
        {
            Print _print = delegate (int val)
            {
                Console.WriteLine("Inside Anonymous method. Value: {0}", val);
            };
            _print(100);
            Console.WriteLine("Hello World!");
        }   
    }
}
