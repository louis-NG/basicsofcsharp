﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DelegatesProject
{
    class MathUtils
    {
        public delegate float TaxFormula(float salary);
        public static float getTaxUSA(float salary)
        {
            return 10 * salary / 100;
        }
        public static float getTaxVN(float salary)
        {
            return 30 * salary / 100;
        }
        public static float getTaxDefault(float salary)
        {
            return 50 * salary / 100;
        }
        public static TaxFormula getTaxOfCodeCountry(string code) 
        {
            if (code == "VN")
            {
                return MathUtils.getTaxVN;
            }
            else if (code == "USA")
            {
                return MathUtils.getTaxUSA;
            }
            else return MathUtils.getTaxDefault;
        }
    }
}
