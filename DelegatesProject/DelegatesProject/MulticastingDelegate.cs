﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DelegatesBasic
{
    class MulticastingDelegate
    {
        public static void Hello(string name)
        {
            Console.WriteLine("Hello " + name);
        }
        public static void Bye(string name)
        {
            Console.WriteLine("Bye " + name);
        }
    }
}
