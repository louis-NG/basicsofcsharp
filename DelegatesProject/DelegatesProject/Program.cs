﻿using System;

namespace DelegatesProject
{
    class Program
    {
        private delegate float delegateTax(float salary);
        static void Main(string[] args)
        {
            //  Delegate representation function.
            float salary = 1000f;
            delegateTax delegatetax = new delegateTax(MathUtils.getTaxDefault); 
            Console.WriteLine("Tax USA salary:{0:n2}, tax:{1:n2}", salary, delegatetax(salary));

            // Delegate can create func return func
            string code = "VN";
            MathUtils.TaxFormula taxFormula = MathUtils.getTaxOfCodeCountry(code);
            Console.WriteLine("Tax VN salary:{0:n2}, tax:{1:n2}", salary, taxFormula(salary));


        }
    }
}
