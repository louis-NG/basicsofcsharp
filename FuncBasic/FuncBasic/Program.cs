﻿using System;

namespace FuncBasic
{
    class Program
    {
        static int Sum(int a , int b)
        {
            return a + b;
        }
        static void Main(string[] args)
        {
            Func<int, int, int> add = Sum;
            int sum = add(1, 5);
            // Func with delegate.
            Func<int> getRandom = delegate ()
            {
                Random rd = new Random();
                return rd.Next(1, 100);
            };
            // Func with Lambda.
            Func<int> getRandomNumber = () => { return new Random().Next(1, 100); };
            Func<int, int, int> SumLambda = (x, y) => { return x + y; };

            Console.WriteLine(sum);
            Console.WriteLine(getRandom());
            Console.WriteLine(getRandomNumber());
            Console.WriteLine(SumLambda(1,2));
        }
    }
}
