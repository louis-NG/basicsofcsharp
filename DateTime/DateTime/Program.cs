﻿using System;
using System.Collections.ObjectModel;

namespace DateTimeApp
{
    class Program
    {
        public static void getDateTime()
        {
            DateTime localTime1 = DateTime.Now;
            //localTime1 = DateTime.SpecifyKind(localTime1, DateTimeKind.Local);
            DateTimeOffset localTime2 = localTime1;
            Console.WriteLine("Converted {0} {1} to a DateTimeOffset value of {2}",
                              localTime1,
                              localTime1.Kind.ToString(),
                              localTime2);
        }
        private static void ShowPossibleTimeZones(DateTimeOffset offsetTime)
        {
            TimeSpan offset = offsetTime.Offset;
            ReadOnlyCollection<TimeZoneInfo> timeZones;

            Console.WriteLine("{0} could belong to the following time zones:",
                              offsetTime.ToString());
            // Get all time zones defined on local system
            timeZones = TimeZoneInfo.GetSystemTimeZones();
            // Iterate time zones 
            foreach (TimeZoneInfo timeZone in timeZones)
            {
                // Compare offset with offset for that date in that time zone
                if (timeZone.GetUtcOffset(offsetTime.DateTime).Equals(offset))
                    Console.WriteLine("   {0}", timeZone.DisplayName);
            }
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            DateTime thisDate = new DateTime(2007, 3, 10, 0, 0, 0);
            DateTime dstDate = new DateTime(2007, 6, 10, 0, 0, 0);
            DateTimeOffset thisTime;

            thisTime = new DateTimeOffset(dstDate, new TimeSpan(-7, 0, 0));
            ShowPossibleTimeZones(thisTime);

            thisTime = new DateTimeOffset(thisDate, new TimeSpan(-6, 0, 0));
            ShowPossibleTimeZones(thisTime);

            thisTime = new DateTimeOffset(thisDate, new TimeSpan(+1, 0, 0));
            ShowPossibleTimeZones(thisTime);


            // TIMESPAN
            // Instantiate a StoreInfo object.
            var store103 = new StoreInfo();
            store103.store = "Store #103";
            store103.tz = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            // Store opens at 8:00.
            store103.open = new TimeSpan(8, 0, 0);
            // Store closes at 9:30.
            store103.close = new TimeSpan(21, 30, 0);

            Console.WriteLine("Store is open now at {0}: {1}",
                              DateTime.Now.TimeOfDay, store103.IsOpenNow());
            TimeSpan[] times = { new TimeSpan(8, 0, 0), new TimeSpan(21, 0, 0),
                           new TimeSpan(4, 59, 0), new TimeSpan(18, 31, 0) };
            foreach (var time in times)
                Console.WriteLine("Store is open at {0}: {1}",
                                  time, store103.IsOpenAt(time));
        }
    
    }
}
