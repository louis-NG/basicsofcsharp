﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExtensionMethod
{
    public static class ExtensionClass
    {
        public static bool IsGreaterThan(this int i, int value)
        {
            return i > value;
        }
    }
}
