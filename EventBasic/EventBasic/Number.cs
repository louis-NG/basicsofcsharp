﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventBasic
{
    class Number
    {
        private PrintHelpler _printHelpler;
        private int _value { get; set; }
        public Number(int val)
        {
            _value = val;
            _printHelpler = new PrintHelpler();
            _printHelpler.beforePrintEvent += printHelper_beforePrintEvent;
        }
        //beforePrintevent handler
        void printHelper_beforePrintEvent()
        {
            Console.WriteLine("BeforPrintEventHandler: PrintHelper is going to print a value");
        }
        public void PrintMoney()
        {
            _printHelpler.PrintMoney(_value);
        }

        public void PrintInteger()
        {
            _printHelpler.PrintInteger(_value);
        }

    }
}
