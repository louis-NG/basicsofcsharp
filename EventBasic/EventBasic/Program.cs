﻿using System;

namespace EventBasic
{
    class Program
    {
        static void Main(string[] args)
        {
            Number _number = new Number(9999);
            _number.PrintInteger();
            _number.PrintMoney();
            Console.WriteLine("Hello World!");
        }
    }
}
